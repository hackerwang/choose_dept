new Image().src="decode.png";
var start, showDecode, jumpToDecode, lastTime, lastAcc, isStarted = false;

start = function() {
	isStarted = true;
	showDecode();
}

showDecode = function(){
	$('.decode').show();
	setTimeout(jumpToDecode, 3000);
}

jumpToDecode = function(){
	var urls = [
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208755534&idx=1&sn=e88e0e0f7cff43ba8836a61d9219fb2d#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208755464&idx=1&sn=55db13a6853b856087a752bcd622eda4#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754695&idx=1&sn=c23b9cc62c7671368254b0a469afe6dd#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754653&idx=1&sn=371ab9abaf39bde513d5a96775f954dd#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754617&idx=1&sn=46123c496e44fe181e8c02b9f4c7498a#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754598&idx=1&sn=76177aee1b46a027daf352d7b3c34709#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754554&idx=1&sn=1e40ce9d378963227ccfcefaa3808970#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754540&idx=1&sn=6685544d8da55d7e35456aa76b245aa4#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754516&idx=1&sn=bffc2686e245c97b300eb21134693415#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754360&idx=1&sn=5258d6c2c1e6172012421fd204d8dfd5#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754379&idx=1&sn=c3a27ff38c9c4b32fc441e4d19afe78e#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754402&idx=1&sn=79a1b31d8b4c0ab393a2ba14e26bcf3c#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754328&idx=1&sn=808c952ce8bbd6efb6282760482eab10#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754292&idx=1&sn=fef3d6acdddf621777e30ae212a164b8#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208754240&idx=1&sn=6639da143a1deea7d30e6d89f4edf2fa#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208752143&idx=1&sn=e5976b2c05a8b8e2021951de7eec5a6d#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208750640&idx=1&sn=6acaef8f6515f338430f784e4a9961b4#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208749969&idx=1&sn=71bbef032c519a6239fbd60c7b16e9bd#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208755662&idx=1&sn=70727378a52633b5d79b829a8f04ed14#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208755640&idx=1&sn=d6daa246ce0cb53d5293bc7d2bd12d7b#rd",
	"http://mp.weixin.qq.com/s?__biz=MzA3NTAwMDczOA==&mid=208755596&idx=1&sn=6b1cf700c7be4a22da7c414f6ca8872a#rd"
	];
	var jumpTo = urls[parseInt(Math.random() * urls.length)];
	window.location = jumpTo;
}

$('.do').click(start);

//摇一摇
$(window).on('deviceorientation', function(e) {
	//初始化完成后有的浏览器可能误触发，阻塞400ms
	var curTime = new Date().getTime(); 
	if (curTime - initTime < 400)
	{
		return false;
	}
	//已经开始则禁用
	if (isStarted) {
		return false;
	}
	if (!lastAcc) {
		lastAcc = e;
		return true;
	}
	var speed = e.alpha + e.beta + e.gamma - lastAcc.alpha - lastAcc.beta - lastAcc.gamma;
	if (Math.abs(speed) > 100) {
		start();
	}
	lastAcc = e;
});
